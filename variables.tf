variable "site_name" {
  type        = string
  description = "Enter the website name (which is also the bucket name)."
}

variable "acl" {
  type        = string
  description = "Make bucket accessible to the public."
  default     = "public-read"
}

variable "index_document_name" {
  type        = string
  description = "The index.html document name used for the home page. By default, this is index.html which can be changed to fit your site. You can specify this document to override the dfault name."
  default     = "index.html"
}

variable "error_document_name" {
  type        = string
  description = "The error.html page displayed when an error occurs"
  default     = "error.html"
}

variable "region" {
  type        = string
  description = "region our bucket/resource is created."
  default     = "us-east-2"
}