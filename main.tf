
resource "aws_s3_bucket" "demo_site" {
  bucket = var.site_name
}

resource "aws_s3_bucket_acl" "site" {
  bucket = aws_s3_bucket.demo_site.id

  acl = var.acl
}

resource "aws_s3_bucket_policy" "site" {
  bucket = aws_s3_bucket.demo_site.id

  policy = jsonencode({
    Version = "2012-10-17"
    Statement = [
      {
        Sid       = "PublicReadGetObject"
        Effect    = "Allow"
        Principal = "*"
        Action = [
          "s3:GetObject",
          "s3:PutObject"
        ]
        Resource = [
          aws_s3_bucket.demo_site.arn,
          "${aws_s3_bucket.demo_site.arn}/*",
        ]
      },
    ]
  })
}

//The enable s3 website with default index.html and error.html pages
resource "aws_s3_bucket_website_configuration" "site" {
  bucket = aws_s3_bucket.demo_site.id

  index_document {
    suffix = var.index_document_name
  }

  error_document {
    key = var.error_document_name
  }
}

// Allow cors configuration for uploads
resource "aws_s3_bucket_cors_configuration" "this" {
  bucket = aws_s3_bucket.demo_site.id

  cors_rule {
    //Cors rule can be updated to fit your site.
    allowed_headers = ["*"]
    allowed_methods = ["PUT", "POST", "GET", "DELETE", "HEAD"]
    allowed_origins = ["http://${var.site_name}","http://*.${var.site_name}"]
    expose_headers  = []
  }
}