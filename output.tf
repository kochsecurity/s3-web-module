output "web_name" {
  value = aws_s3_bucket.demo_site.bucket
}

output "iam_arn" {
  value = aws_s3_bucket_policy.site.id
}