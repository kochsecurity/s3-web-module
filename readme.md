
# S3 upload.

Since this is a module, we uploaded the files we need in the bucket separatelt in the CI process or manually using the command
below once the bucket is already created with this module.

```
aws s3 cp . s3://demo.dev.com --exclude "*" --include "*.html"  --recursive  
```

```
Completed 396 Bytes/396 Bytes (1.8 KiB/s) with 1 file(s) remaining
upload: ./index.html to s3://demo.dev.com/index.html              
```

To delete contents from an s3 bucket command.

```
aws s3 rm s3://demo.dev.com --recursive
```

## Input required

    site_name = "my-site-name.com"


## tag release 

You can also use command line tools like git to add tags to your repository, you can use this command:

```git tag -a v1.0.0 -m "Initial release of mymodule"```

 and then 
 
 ```git push --tags``` 
 
 to push the tags to your repository.

It's important to note that the naming of the tag can vary depending on your internal conventions, but the semantic versioning format is a common practice.

## reference modules

```
module "s3_bucket" {
  source = "git@github.com:kochsecurity/s3-web-module.git//v1.0.0"
}
```

OR

// This option worked

```
module "s3_bucket" {
  source = "git@github.com:kochsecurity/s3-web-module.git?ref=v1.0.0"
}
```
